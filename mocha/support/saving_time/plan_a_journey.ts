import {browser, by, element, protractor} from 'protractor';

const EC = protractor.ExpectedConditions;

const NAV_BUTTON_CSS = "#searchbox-directions";
const TRAIN_BUTTON_CSS = ".directions-travel-mode-icon.directions-transit-icon";
const FROM_INPUT_CSS = "#sb_ifc51 .tactile-searchbox-input";
const TO_INPUT_CSS = "#sb_ifc52 .tactile-searchbox-input";


export function PlanAJourney() {
    browser.get('/maps');

    let navButtonElement = element(by.css(NAV_BUTTON_CSS));
    browser.wait(EC.visibilityOf(navButtonElement), 5000);
    navButtonElement.click();

    let trainButtonElement = element(by.css(TRAIN_BUTTON_CSS));
    browser.wait(EC.visibilityOf(trainButtonElement), 5000);
    trainButtonElement.click();
}

export function ChooseOrigin(origin: string) {
    let fromInputElement = element(by.css(FROM_INPUT_CSS));
    browser.wait(EC.visibilityOf(fromInputElement), 5000);
    return fromInputElement.sendKeys(origin);
}

export function ChooseDestination(destination: string) {
    let toInputElement = element(by.css(TO_INPUT_CSS));
    browser.wait(EC.visibilityOf(toInputElement), 5000);
    toInputElement.sendKeys(destination);
    return toInputElement.sendKeys(protractor.Key.RETURN);
}
