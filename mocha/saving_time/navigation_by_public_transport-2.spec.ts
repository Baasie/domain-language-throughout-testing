import {browser, by, element, protractor} from 'protractor';
import {ChooseDestination, ChooseOrigin, PlanAJourney} from '../../cucumber/support/saving_time/plan_a_journey';
import {JourneyList} from '../support/saving_time/journey_list';

const expect = global['chai'].expect;

const TRAIN_LIST_CSS = ".section-listbox";

describe('Navigation by public transport', () => {

    before(() => {
        // Setup environment data and stuff........
    })

    it('Navigate by public transport', () => {

        PlanAJourney();
        ChooseOrigin('Utrecht Centraal');
        ChooseDestination('Amsterdam Centraal');

        expect(JourneyList.Train_List.isPresent()).to.eventually.be.true;

    })
});