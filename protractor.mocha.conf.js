
exports.config = {
    allScriptsTimeout: 11000,
    specs: [
        './mocha/**/*.spec.ts'
    ],
    baseUrl: 'https://www.google.com/',
    capabilities: {
        'browserName': 'chrome'
    },
    directConnect: true,
    framework: 'mocha',
    mochaOpts: {
        reporter: "spec",
        ui: 'bdd',
        timeout: 30000
    },
    beforeLaunch: function() { // If you're using type script then you need compiler options
        require('ts-node').register({
            project: 'tsconfig.json'
        });
    },
    onPrepare: function() { // making chai available globally. in your test use `const expect = global['chai'].expect;`
        var chai = require('chai');
        var chaiAsPromised = require("chai-as-promised");
        chai.use(chaiAsPromised);
        global.chai = chai;
        browser.waitForAngularEnabled(false);
    }
};