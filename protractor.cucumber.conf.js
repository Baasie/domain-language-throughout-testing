exports.config = {
    allScriptsTimeout: 11000,
    specs: [
        'cucumber/**/*.feature'
    ],
    baseUrl: 'https://www.google.com/',
    capabilities: {
        browserName: 'chrome',
    },
    directConnect: true,
    // https://github.com/protractor-cucumber-framework/protractor-cucumber-framework#uncaught-exceptions
    ignoreUncaughtExceptions: true,


    framework: 'custom',
    // path relative to the current config file
    frameworkPath: require.resolve('protractor-cucumber-framework'),

    cucumberOpts: {
        require:    [ 'cucumber/**/*.ts' ],
        format:     'pretty',
        compiler:   'ts:ts-node/register'
    },

    onPrepare: function() {// making chai available globally. in your test use `const expect = global['chai'].expect;`
        var chai = require('chai');
        var chaiAsPromised = require("chai-as-promised");
        chai.use(chaiAsPromised);
        global.chai = chai;
        browser.ignoreSynchronization = true;  // <-- Disables angular synchronisation
    }
};