Feature: Navigation by public transport

  As a commuter,
  I’d like to know the fastest route between two places,
  So that I get to my destination as quickly as possible

  Scenario: Navigate by public transport

    Given Intercity trains from Utrecht CS leave at 16:58, 17:08, 17:18
    When Connie wants to travel from Utrecht CS to Amsterdam CS at 17:00
    Then she should be told about the trains departing at 17:08, 17:18
