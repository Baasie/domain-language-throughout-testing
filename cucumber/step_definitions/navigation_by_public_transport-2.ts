import {ChooseDestination, ChooseOrigin, PlanAJourney} from '../support/saving_time/plan_a_journey';
import {JourneyList} from '../../mocha/support/saving_time/journey_list';

const expect = global['chai'].expect;

const TRAIN_LIST_CSS = ".section-listbox";

export = function myStepDefinitionsWrapper() {

    this.Given(/^Intercity trains from Utrecht CS leave at 16:58, 17:08, 17:18$/, function (callback) {
        // Setup environment data and stuff........
        callback();
    });
    this.When(/^Connie wants to travel from Utrecht CS to Amsterdam CS at 17:00$/, function () {
        PlanAJourney();
        ChooseOrigin('Utrecht Centraal');
        return ChooseDestination('Amsterdam Centraal');

    });
    this.Then(/^she should be told about the trains departing at 17:08, 17:18$/, function () {
        return expect(JourneyList.Train_List.isPresent()).to.eventually.be.true;
    });
};
